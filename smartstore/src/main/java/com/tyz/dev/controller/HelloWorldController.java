package com.tyz.dev.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

    @RequestMapping("/index")
    @ResponseBody
    public ModelAndView helloWorld() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/index.html");
        return modelAndView;
    }

    @RequestMapping("/list")
    @ResponseBody
    public Map userList() {
        Map<String,Object> user = new HashMap<>();
        user.put("id","1dasdas56789");
        user.put("name","123");
        user.put("sex","123");
        user.put("age",123);
        return user;
    }
}
 